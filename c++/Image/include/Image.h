/**
 * @file
 * @author Antoine Millon, Elliott Perryman
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * The Image class holds all the images and manipulations to solve
 *  the problems our group worked on.
 */

#ifndef IMAGE_H
#define IMAGE_H

// general includes
#include <iostream>
#include <opencv2/opencv.hpp>
#include <fftw3.h>
#include <iomanip>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/QR>
#include <stdint.h>
#include <stdlib.h>
#include <list>
#include <time.h>

#define PI 3.14159265358979

/**
 * @brief The Image class
 *
 * The image class has initializers that use opencv to read in images. It then has 
 *  manipulations for drawing rectangles, fitting, filtering, convolution, and producing
 *  the output of our project 
 */
class Image
{
	private:
		//! The opencv matrix of the image	
		cv::Mat mat; 
		//! the number of pixel rows
		unsigned int height; 
		//! the number of pixel cols	
		unsigned int width; 

	public:
		//! initialize the image from the path to an image file 
		Image(std::string);
		//! copy constructor
		Image(const Image&);
		//! initialize an image with a height and width
		Image(unsigned int height, unsigned int width);
		//! calculate the quantile at the level q (0-100)
		double quantile(double q) const;
		//! threshold the input image at threshold t 
		void threshold(Image&, double t) const;
		//! estimate the center of the fingerprint at threshold t (values returned via pointer)
		void threshold_quantile_spot_deduction(
				double t, unsigned int* spot_x, unsigned int* spot_y) const;
		//! calc the min/max and arg min/max of the image
		void minMax(
				double* min, double* max, unsigned int* xmin, 
				unsigned int* ymin, unsigned int* xmax, unsigned int* ymax);
		//! convert matrix to double precision
		void cast2double();
		//! convert matrix to int precision
		void cast2uint();
		//! show the image
		void print();
		//! setter for single pixel
		void setmat(unsigned int x, unsigned int y, double val);
		//! getter for single pixel
		double getmat(unsigned int x, unsigned int y) const;
		//! getter for height 
		unsigned int getheight() const;
		//! getter for width 
		unsigned int getwidth() const;
		//! saves image to file path 
		void save(std::string);
		//! flip matrix vertically (c=0), horizontally (c>0), or both (c<0) 
		void symmetry(int c);
		//! draw circle centered at x, y with radius r
		void drawcircle(unsigned int x, unsigned int y, unsigned int r);
		//! draw rectangle from x0,y0 to x1,y1 with value val
		void drawrect(
				unsigned int x0, unsigned int y0, 
				unsigned int x1, unsigned int y1, double val);
		//! fill ellipse circumscribed by r with value c
		void drawellipse(const cv::RotatedRect r, double c);
		// draw contour TODO	
		void drawcontours(std::vector<std::vector<cv::Point>>, unsigned int, double);
		// reverse colors black and white
		void reverseblack_white();
		// TODO 		
		void apply_bump(Image &, double (*bump_function)(double), double (*norm)(cv::Point2d, cv::RotatedRect), unsigned int, double, bool output=false, std::string output_dir="") const;

		//! simulate erosion on dst image of size s and type t
		void erosion(Image &, unsigned int , int ) const;
		//! simulate dilation on dst image of size s and type t TODO anchor
		void dilation(Image &, unsigned int , int , unsigned int , unsigned int ) const;

		//! find ellipse that approximates fingerprint border with erosion of size s
		cv::RotatedRect ellipse_interpolation(unsigned int, bool output=false, std::string output_dir ="") const;

		//! apply m as filter on image dest
		void apply_filter(Image&, cv::Mat) const;
		//! apply sharpen filter on image dest
		void apply_filter_clean3x3(Image &) const ;
		//! restore image TODO 
		void restoration(Image&, std::string, unsigned int, unsigned int, unsigned int, double, double, bool=false, std::string output_dir = "") const;

		// TODO
		void NXOR(Image&, const Image&) const;
		// TODO
		friend cv::Point2i min_dist_patch(Image& dst, const Image& patch_image, const Image& mask, const std::vector<cv::Point2i>* patch_dict, \
				cv::Point2i actual_patch, unsigned int patch_size);

		// TODO
		//! convolve kxk kernel with image, output a new image (optionally using fft)
		Image* convolve(double* kernel, int k, bool use_fft=false);
		//! deconvolve kxk kernel with image, output a new (cleaned) image 
		Image* deconvolve(double* kernel, int k);
		//! rotate image by angle degrees counter-clockwise	
		Image* rotate(double angle);
};
//! calculate nxn translating and blurring gaussian kernel
double* gaussian_kernel(int n, int x0, int y0, double sigma);

// TODO are these helpers or should they be interfaces
double gaussian(double);

double euclidean_norm(unsigned int, unsigned int, unsigned int, unsigned int);

double elliptique_norm(cv::Point2d, cv::RotatedRect);

cv::Point2d rotate_vect(cv::Point2d, float);

std::vector<cv::Point2i>* patch_creator(const Image&, unsigned int, unsigned int, cv::RotatedRect);

Image* restoration_mask_creator_square(const Image* src);

Image* restoration_mask_creator(const Image* src, cv::RotatedRect, unsigned int, bool, std::string, double, double);

double matrix_patch_dist(const Image& dst, const Image& patch_image, const Image& mask, unsigned int patch_size,\
		cv::Point2i patch, cv::Point2i actual_patch);

std::vector<cv::Point2i>* priority_creator(const Image& mask, cv::RotatedRect rect);

bool patch_coord_comparision(cv::Point2i p1, cv::Point2i p2);


double* img_to_c_arr(cv::Mat m);
cv::Mat c_arr_to_img(double *arr, int M, int N);
double* convolution_manual(double *in, double *kernel, int M, int N, int k) ;
double* convolution_fft(double *in, double *kernel, int M, int N, int k);
double* deconvolution(double* kernel, double *blur, int M, int N, int k);
double *rotate_image(cv::Mat data, double angle);
#endif
