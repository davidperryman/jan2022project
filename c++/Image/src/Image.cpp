/**
 * @file
 * @author Antoine Millon, Elliott Perryman
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * The Image class holds all the images and manipulations to solve
 *  the problems our group worked on.
 */

#include "../include/Image.h"

double gaussian(double r) {
	return exp(- pow(r, 25));
}

double euclidean_norm(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2) {
	return pow(pow((int)x1 - (int)x2, 2) + pow((int)y1 - (int)y2, 2), 0.5);
}

double elliptique_norm(cv::Point2d p, cv::RotatedRect rect) {
	double radian_angle, ellipse_a, ellipse_b, x_scale;
	cv::Point2d ellipse_center;
	radian_angle = rect.angle*PI / 180;
	ellipse_a = rect.size.width / 2;
	ellipse_b = rect.size.height / 2;
	ellipse_center = {rect.center.y, rect.center.x}; //y and x are inverted with our convention


	x_scale = pow(ellipse_a, 2) / pow(ellipse_b, 2);
	
	p -= ellipse_center;
	p = rotate_vect(p, radian_angle);
	//Then we do the opposite translation
	p += ellipse_center ;
	//We compute the distance between the center of ellipse and the point (x,y)	
	return pow(x_scale*pow(p.x - ellipse_center.x, 2) + pow(p.y - ellipse_center.y, 2), 0.5);
}

cv::Point2d rotate_vect(cv::Point2d p, float radian_angle) {
	return cv::Point2d(p.x * cos(radian_angle) - p.y * sin(radian_angle), p.x * sin(radian_angle) + p.y * cos(radian_angle));
}

// * \param[out]    b    Description of the parameter b.
// * \param[in,out] c    Description of the parameter c.
// * \return        The error return code of the function.
// *
// * \retval        ERR_SUCCESS    The function is successfully executed
// * \retval        ERR_FAILURE    An error occurred

/**
 * \brief   Image constructor with file path
 *
 * \details Constructs an Image class from the grayscale image file at location image_path. 
 * 	If the file is not able to be found at image_path, it prints to stderr and exits -1.
 *
 * \note    This function calls exit(-1) on failure
 *
 * \param[in]     image_path    a std::string pointing to the file path
 */
Image::Image(std::string image_path) {
	mat = imread(image_path, cv::IMREAD_GRAYSCALE);
	if (mat.empty())
	{
		std::cerr << "Could not read the image: " << image_path << std::endl;
		exit(-1);
	}
	height = mat.size().height;
	width = mat.size().width;
}
/**
 * \brief   Image copy constructor
 *
 * \details Constructs an Image class from another Image, using opencv Mat.clone. This 
 * 	duplicates memory
 *
 * \param[in]     img    an Image to be duplicated
 */
Image::Image(const Image& img) {
	mat = img.mat.clone();
	height = img.height;
	width = img.width;
}
/**
 * \brief   Image constructor with no matrix provided 
 *
 * \details Constructs a height x width Image with no data
 *
 * \param[in]     height_    the number of rows 
 * \param[in]     width_    the number of cols 
 */
Image::Image(unsigned int height_, unsigned int width_) : mat(height_, width_, CV_64FC1) {
	height = height_;
	width = width_;
}
/**
 * \brief   Calculate the qth quantile of the image's values
 *
 * \details   Calculate the qth quantile of the image's values. Used for calculating 
 * 	thresholds, e.g. creating binary masks at threshold = quantile (40%).
 *
 * \param[in]     q    the percentage the quantile should be calculated at
 * \return        the quantile at level q
 */
double Image::quantile(double q) const{
	std::list<double> l;
	for (unsigned int x = 0; x < height; x++) {
		for (unsigned int y = 0; y < width; y++) {
			l.push_back(getmat(x, y));
		}
	}
	l.sort();
	unsigned int q_index = ceil(l.size() * q);
	auto front = l.begin();
	advance(front, q_index);
	return *front;
}
/**
 * \brief   Convert image to 0s and 1s if pixels are below or above threshold 
 *
 * \details   for every pixel, if it is <= threshold, set it to 0, otherwise, set it to 1
 *
 * \param[out]     dst		the image to write the thresholded values to
 * \param[in]      threshold		the value to compare pixels against
 */
void Image::threshold(Image &dst, double threshold) const{
	for (unsigned int x = 0; x < height; x++) {
		for (unsigned int y = 0; y < width; y++) {
			if (getmat(x, y) <= threshold) {
				dst.setmat(x, y, 0);
			}
			else {
				dst.setmat(x, y, 1);
			}
		}
	}
}
/**
 * \brief   calculate the center of mass of the instance
 *
 * \details   this takes an image, thresholds it, then calculates a center
 * 	(spot_x,spot_y) using the center of mass
 *
 * \param[in]		threshold	the threshold to make the binary image
 * \param[out]		spot_x		a pointer that will be given the x coordinate of the center
 * \param[out]		spot_y		a pointer that will be given the y coordinate of the center
 *
 */
void Image::threshold_quantile_spot_deduction(double threshold, unsigned int* spot_x, unsigned int* spot_y) const {
	Image mask(getheight(), getwidth());

	this->threshold(mask, threshold);

	std::list<double> l_x;
	std::list<double> l_y;
	for (unsigned int x = 0; x < height; x++) {
		for (unsigned int y = 0; y < width; y++) {
			if (mask.getmat(x, y) == 0) {
				l_x.push_back(x);
				l_y.push_back(y);
			}
		}
	}
	//Center of mass computation
	double sum_x = 0;
	double sum_y = 0;

	for (auto const& elem : l_x) {
		sum_x += elem;
	}
	for (auto const& elem : l_y) {
		sum_y += elem;
	}
	sum_x /= l_x.size();
	sum_y /= l_y.size();
	*spot_x = (unsigned int)round(sum_x);
	*spot_y = (unsigned int)round(sum_y);
}
/**
 * \brief   Calculate the min, max, argmin, argmax of an image
 *
 * \param[out]		min		a pointer that will be given the value of the minimum
 * \param[out]		max		a pointer that will be given the value of the maximum
 * \param[out]		xmin		a pointer that will be given the x coordinate of the minimum
 * \param[out]		ymin		a pointer that will be given the y coordinate of the minimum
 * \param[out]		xmax		a pointer that will be given the x coordinate of the maximum
 * \param[out]		ymax		a pointer that will be given the y coordinate of the maximum
 *
 */
void Image::minMax(double* min, double* max, unsigned int* xmin, unsigned int* ymin, unsigned int* xmax, unsigned int* ymax) {
	*min = 256;
	*max = -1;
	double act_val;
	for (unsigned int x = 0; x < height; x++) {
		for (unsigned int y = 0; y < width; y++) {
			act_val = getmat(x, y);
			if (*min > act_val) {
				*min = act_val;
				*xmin = x;
				*ymin = y;
			}
			if (*max < act_val) {
				*max = act_val;
				*xmax = x;
				*ymax = y;
			}
		}
	}
}
/**
 * \brief	Convert an image to double precision 
 *
 */
void Image::cast2double() {
	mat.convertTo(mat, CV_64FC1, 1 / 255.0);  // third arg is the scale factor
}
/**
 * \brief	Convert an image to integer precision 
 *
 */
void Image::cast2uint() {
	mat.convertTo(mat, CV_8UC1, 255);
}
/**
 * \brief	print an image and wait for keystroke	
 *
 */
void Image::print() {
	if (mat.type() == CV_64FC1) {
		cast2uint();
		cv::imshow("Finger Print", mat);
		cast2double();
	}
	else {
		cv::imshow("Finger Print", mat);
	}
	cv::waitKey(0); //Wait for a keystroke in the window
};
/**
 * \brief   setter for a single pixel
 *
 * \details 	set the pixel at x,y to value
 *
 */
void Image::setmat(unsigned int x, unsigned int y, double value) {
	mat.at<double>(x, y) = value;
}
/**
 * \brief   getter for a single pixel
 *
 * \details 	get the value at pixel x,y
 *
 * \returns	value	the value at pixel x,y
 */
double Image::getmat(unsigned int x, unsigned int y) const {
	return mat.at<double>(x, y);
}
/**
 * \brief   get the image height (number of rows)
 *
 * \returns	height		the height of the image
 */
unsigned int Image::getheight() const {
	return height;
}
/**
 * \brief   get the image width (number of cols)
 *
 * \returns	width		the width of the image
 */
unsigned int Image::getwidth() const {
	return width;
}
/**
 * \brief   save an image to filename
 *
 * \param[in] 	filename	a string file path+name, eg. std::string("../img.png") 
 */
void Image::save(std::string filename) {

	if (mat.type() == CV_64FC1) {
		cast2uint();
		imwrite(filename, mat);
		cast2double();
	}
	else {
		imwrite(filename, mat);
	}
}
/**
 * \brief	flip an image by x, y, or x and y axis 
 *
 * \param[in] 	code	code for which axis to flip. code=0 => vertical flip. code >0 => horizontal flip. code<0 => horizontal and vertical flip. 
 */
void Image::symmetry(int code) {
	flip(mat, mat, code);
}
/**
 * \brief	draw a circle on the image	
 *
 * \param[in] 	x	the x coordinate of the circle center
 * \param[in] 	y	the y coordinate of the circle center
 * \param[in]	r	the radius of the circle
 */
void Image::drawcircle(unsigned int x, unsigned int y, unsigned int r) {
	circle(mat, cv::Point(x, y), r, 50, -1);
}
/**
 * \brief	draw a rectangle on the image	
 *
 * \param[in] 	xtl	the x coordinate of the top left of the rectangle
 * \param[in] 	ytl	the y coordinate of the top left of the rectangle
 * \param[in] 	xbr	the x coordinate of the bottom right of the rectangle
 * \param[in] 	ybr	the y coordinate of the bottom right of the rectangle
 * \param[in] 	c	the value to fill the rectangle with
 */
void Image::drawrect(unsigned int xtl, unsigned int ytl, unsigned int xbr, unsigned int ybr, double c) {
	for (unsigned int x = xtl; x <= xbr; x++) {
		for (unsigned int y = ytl; y <= ybr; y++) {
			setmat(x, y, c);
		}
	}
}
// TODO
/**
 * \brief 	draw contours
 * \details 	
 * \param[in]	tab	tab is
 * \param[in] 	idx	
 * \param[in] 	color		
 */
void Image::drawcontours(std::vector<std::vector<cv::Point>> tab, unsigned int idx, double color){
	drawContours(this->mat, tab, idx, color, 2);
}
/**
 * \brief 	draw an ellipse on the image
 * \details 	fill the ellipse circumscribed by the rectangle r with the value c
 * \param[in]	r	the rectangle that the oval circumscribes
 * \param[in] 	c	the value to fill the ellipse with
 */
void Image::drawellipse(const cv::RotatedRect r, double c) {
	ellipse(this->mat, r, c, 4, 1);
}
/** \brief reverse black and white
*/
void Image::reverseblack_white() {
	for (unsigned int x = 0; x < height; x++) {
		for (unsigned int y = 0; y < width; y++) {
			setmat(x, y, 1 - getmat(x, y));
		}
	}
}
// TODO
/**
 * \brief 		
 * \details 	
 * \param[out]	dst	the output image
 * \param[in]	erosion_size	
 * \param[in]	erosion_type
 */
void Image::erosion(Image &dst, unsigned int erosion_size, int erosion_type) const{
	cv::Mat element = getStructuringElement( erosion_type, cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ), cv::Point(erosion_size, erosion_size));
	erode(this->mat, dst.mat, element);
}
// TODO
/**
 * \brief 		
 * \details 	
 * \param[out]	dst	the output image
 * \param[in]	erosion_size	
 * \param[in]	erosion_type
 * \param[in]	x_anchor	
 * \param[in]	y_anchor	
 */
void Image::dilation(Image &dst, unsigned int erosion_size, int erosion_type, unsigned int x_anchor, unsigned int y_anchor) const{
	cv::Mat element = getStructuringElement( erosion_type, cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ), cv::Point(erosion_size, erosion_size));
	dilate(this->mat, dst.mat, element);
}

// TODO
/**
 * \brief 		apply a bump function on the Image
 * \details 	
 * \param[out]	dst	the output image
 * \param[in]	bump_function	
 * \param[in]	norm
 * \param[in]	erosion_size	
 */





// TODO

void Image::apply_bump(Image &dst, double (*bump_function)(double), double (*norm)(cv::Point2d, cv::RotatedRect),\
unsigned int erosion_size, double alpha, bool output, std::string output_dir) const {
	Image tmp(*this);
	double dist, ellipse_a;
	cv::RotatedRect rect;
	cv::Point2d p;

	if (output)
		tmp.save(output_dir + "apply_bump_step_0_original_image.png");
	//To be consistent with the scaling factor of the bump function
	tmp.reverseblack_white();
	//We reverse the black and the white to avoid contours errors (with findContours)

	rect = this->ellipse_interpolation(erosion_size, output, output_dir);
	ellipse_a = rect.size.width/2;
	for (unsigned int x = 0; x < height; x++) {
		for (unsigned int y = 0; y < width; y++) {
			p = {(double)x, (double)y};
			//We compute the distance between the center of ellipse and the point (x,y)
			dist = norm(p, rect) / (ellipse_a*alpha);
			//cout << "x: " << x << " y: " << y << " p.x: " << p.x << " p.y: " << p.y << " dist: " << dist << endl;
			dst.setmat(x, y, tmp.getmat(x, y) * bump_function(dist));
		}
	}
	dst.reverseblack_white();
	if (output){
		tmp.reverseblack_white();
		dst.save(output_dir + "apply_bump_step_5_FINAL.png");
		dst.drawellipse(rect, 0.5);
		dst.save(output_dir + "apply_bump_step_6_FINAL_ELLIPSE.png");
		tmp.drawellipse(rect, 0.5);
		tmp.save(output_dir + "apply_bump_step_6_ORIGINAL_ELLIPSE.png");

	}
}
/**
 * \brief 	Returns the parameters of an ellipse which is interpolating the image instance
 * \details 	To compute the interpolation we first create a deep copy of the instance then we 
 dilate it, we threshold it, we calculate his contour and then we use an open cv
 function that is interpolating an ellipse given a set of 2D points(our contour).
 We finally return an opencv object that is representing the ellipse (small radius
 , big radius, center, rotation angle).
 * \param[in]	erosion_size	2*erosion_size + 1 is the size of the sqare matrix of the dilation matrix that we use
 * \returns		RotatedRect inscribed rectangle of the interpolating ellipse
 */
cv::RotatedRect Image::ellipse_interpolation(unsigned int erosion_size, bool output, std::string output_dir) const{

	Image tmp(getheight(), getwidth());

	this->erosion(tmp, erosion_size, cv::MORPH_RECT);
	if (output){
		tmp.print();
		tmp.save(output_dir + "apply_bump_step_1_dilatation.png");
	}
	tmp.threshold(tmp, 0.75);

	//In order that findCoutours do not find the borders of the image as a coutour
	tmp.reverseblack_white();
	if (output){
		tmp.print();
		tmp.save(output_dir + "apply_bump_step_2_threshold.png");
	}


	std::vector<std::vector<cv::Point>> contours;

	tmp.cast2uint(); //The findCoutours function of opencv needs to have a binary image.
	//OPEN CV function
	cv::findContours(tmp.mat, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_TC89_L1);

	//Here we are looking for the longest contour
	unsigned int id_max_contour = 0;
	unsigned int tmp_size = 0;
	for (unsigned int i = 0; i < contours.size(); i++){
		if (tmp_size < contours[i].size()){
			id_max_contour = i;
			tmp_size = contours[i].size();
		}
	}

	//We are drawing the biggest contour of the image
	if (output){
		tmp.drawcontours(contours, id_max_contour, 127);
		tmp.print();
		tmp.save(output_dir + "apply_bump_step_3_contours.png");
	}

	//This OPEN CV function finds the best ellipse interpolating our longest contour
	cv::RotatedRect rect = cv::fitEllipse(contours[id_max_contour]);
	
	if (output){
		tmp.drawcircle((unsigned int)rect.center.x, (unsigned int)rect.center.y, 5);
		tmp.drawellipse(rect, 127);
		tmp.print();
		tmp.save(output_dir + "apply_bump_step_4_ellipse.png");

	}
	return rect;
}
/** 
 * \brief 	apply one image as a filter and output to dst 
 * \details 	convolve this image with the kernel, using border reflection and
 * 	sending the output to dst
 * \param[out]	dst	an image to fill in with the output
 * \param[in]	kernel	an image to use as the kernel
 */ 
void Image::apply_filter(Image &dst, cv::Mat kernel) const{
	filter2D(this->mat, dst.mat, -1, kernel, cv::Point(-1, -1), 0, cv::BORDER_REFLECT_101);
}
/** 
 * \brief 	apply the sharpen filter and output to dst 
 * \details 	convolve this image with the 3x3 sharpen kernel, 
 * 	using border reflection and sending the output to dst
 * \param[out]	dst	an image to fill in with the output
 */ 
void Image::apply_filter_clean3x3(Image &dst) const{
	cv::Mat kernel(3, 3, CV_64FC1);
	kernel.at<double>(0,0) = -1; kernel.at<double>(1,0) = -1;
	kernel.at<double>(2,0) = -1; kernel.at<double>(0,1) = -1;
	kernel.at<double>(1,1) = 9; kernel.at<double>(2,1) = -1;
	kernel.at<double>(0,2) = -1; kernel.at<double>(2,2) = -1;
	kernel.at<double>(1,2) = -1; 

	this->apply_filter(dst, kernel);
}
// TODO
//patch_size corresponds to the middle -1 of the real patch size
/**
 * \brief 	draw contours
 * \details 	
 * \param[out]	dst	the output image
 * \param[in] 	src_image	the file path to the source image	
 * \param[in] 	patch_number	
 * \param[in]	patch_size			
 */
void Image::restoration(Image& dst, std::string patch_image_path, unsigned int patch_number, unsigned int patch_size, \
unsigned int mask_ellipse_radius, double alpha, double beta, bool output, std::string output_dir) const{
	dst = *this; //Copying the image to be restored

	//the image from where we are collecting the patches
	Image patch_image(patch_image_path);
	patch_image.cast2double();
	//Point that will store the coordinates of the corresponding best patch of the patch dict
	cv::Point2i best_patch;
	//rect will be the inscribed rectangle of the interpolating ellipse of the image to restore
	cv::RotatedRect rect;
	//list of patches centers in the patch_image AND list order of the coordinates centers to prioritise
	std::vector<cv::Point2i>* patch_dict, *patch_priority;
	//In the mask matrix : 0 if true 1 if false
	Image* mask;

	rect = this->ellipse_interpolation(2);//argument is the dilation size, standard is 2
	patch_dict = patch_creator(patch_image, patch_number, patch_size, rect);
	mask = restoration_mask_creator(this, rect, mask_ellipse_radius, output, output_dir, alpha, beta);
	patch_priority = priority_creator(*mask, rect);


	for (auto patch_center : *patch_priority) {
		best_patch = min_dist_patch(dst, patch_image, *mask,  patch_dict, patch_center, patch_size);
		dst.setmat(patch_center.x, patch_center.y, patch_image.getmat(best_patch.x, best_patch.y));
		//Now the new pixel can be used to compute the distance for other patches
		mask->setmat(patch_center.x, patch_center.y, 1);
	}

	delete patch_dict;
	delete mask; 
	delete patch_priority;
}


bool patch_coord_comparision(cv::Point2i p1, cv::Point2i p2){
	return (euclidean_norm(p1.x, p1.y, 0, 0)  < euclidean_norm(p2.x, p2.y, 0, 0));
}


//priority is to be close to the center of the fingerprint
std::vector<cv::Point2i>* priority_creator(const Image& mask, cv::RotatedRect rect) {
	std::vector<cv::Point2i>* priority = new std::vector<cv::Point2i>();

	for (unsigned int x = 0; x < mask.getheight(); x++) {
		for (unsigned int y = 0; y < mask.getwidth(); y++) {
			if (!mask.getmat(x, y)){
				priority->push_back(cv::Point2i((int)x - (int)rect.center.y, (int)y - (int)rect.center.x));
			}
		}
	}

	std::sort(priority->begin(), priority->end(), patch_coord_comparision);
	//Re translating back
	for (unsigned int i = 0; i < priority->size(); i++){
		*(priority->begin()+i) += cv::Point2i((int)rect.center.y, (int)rect.center.x);
	}
	return priority;
}

//Returns the center of the closest patch of the patch_dict list from
//the actual_patch_p
cv::Point2i min_dist_patch(Image& dst, const Image& patch_image, const Image& mask, const std::vector<cv::Point2i>* patch_dict, \
cv::Point2i actual_patch, unsigned int patch_size) {
	double min_dist = 1000;
	double tmp_dist;
	cv::Point2i best_patch;

	for (auto patch : *patch_dict) {
		tmp_dist = matrix_patch_dist(dst, patch_image, mask, patch_size, patch, actual_patch);
		if (tmp_dist < min_dist) {
			min_dist = tmp_dist;
			best_patch = patch;
		}
	}
	return best_patch;
}

double matrix_patch_dist(const Image& dst, const Image& patch_image, const Image& mask, unsigned int patch_size,\
 cv::Point2i patch, cv::Point2i actual_patch){
	double dist = 0;
	for (int x = -patch_size; x <= (int)patch_size; x++) {
		for (int y = -patch_size; y <= (int)patch_size; y++) {
			//If the pixel is in the mask (TRUE) or the pixel is out of image we do not count it for the norm 
			if (mask.getmat(actual_patch.x + x, actual_patch.y + y) && \
				actual_patch.x + x >= (int)patch_size && actual_patch.x + x < (int)dst.getheight()-(int)patch_size && \
				actual_patch.y + y >= (int)patch_size && actual_patch.y + y < (int)dst.getwidth()-(int)patch_size){

				dist += pow(patch_image.getmat(patch.x + x, patch.y + y) - dst.getmat(actual_patch.x + x, actual_patch.y + y), 2);
			}
		}
	}
	return dist;
}

Image* restoration_mask_creator_square(const Image* src) {
	Image* mask = new Image(src->getheight(), src->getwidth());
	for (unsigned int x = 0; x < src->getheight(); x++) {
		for (unsigned int y = 0; y < src->getwidth(); y++) {
			if (x > 150 && x < 200 && y > 150 && y < 230){
				mask->setmat(x, y, 0);
			}
			else {
				mask->setmat(x, y, 1);
			}
		}
	}
	return mask;
}

Image* restoration_mask_creator(const Image* src, cv::RotatedRect rect, unsigned int mask_ellipse_width,\
 bool output, std::string output_dir, double alpha, double beta) {
	Image* mask = new Image(src->getheight(), src->getwidth());
	mask->cast2double();

	double mask_ellipse_radius = (rect.size.width/2)*alpha + beta;
	for (unsigned int x = 0; x < src->getheight(); x++) {
		for (unsigned int y = 0; y < src->getwidth(); y++) {
			if (elliptique_norm(cv::Point2d((double)x, (double)y), rect) >= mask_ellipse_radius && \
				elliptique_norm(cv::Point2d((double)x, (double)y), rect) < mask_ellipse_radius + mask_ellipse_width){
				mask->setmat(x, y, 0);
			}
			else {
				mask->setmat(x, y, 1);
			}
		}
	}
	if (output){
		mask->print();
		mask->save(output_dir + "_restoration_step_1_mask.png");
	}
	return mask;
}

std::vector<cv::Point2i>* patch_creator(const Image& patch_image, unsigned int patch_number, unsigned int patch_size, \
cv::RotatedRect rect){
	std::vector<cv::Point2i>* patch_dict = new std::vector<cv::Point2i>;
	//Generate the seed
	srand (time(NULL));
	unsigned int actual_patch_number = 0;
	unsigned int x,y;

	//We are looking for the center of patches only in the interpolating ellipse of the fingerprint
	while(actual_patch_number < patch_number){
		x = rand() % (patch_image.getheight() - 2*patch_size) + patch_size;
		y = rand() % (patch_image.getwidth() - 2*patch_size) + patch_size;
		if (elliptique_norm(cv::Point2d((double)x, (double)y), rect) <= rect.size.width/2){
			patch_dict->push_back(cv::Point2i(x, y));
			actual_patch_number += 1;
		}
	}
	patch_dict->push_back(cv::Point2i(patch_size,patch_size));
	return patch_dict;
}

// -- STUFF FOR LINEAR FILTERING -----------------------------
double* gaussian_kernel(int n, int x0, int y0, double sigma) {
	double *kernel = new double[n*n];
	double r;	
	long double total = 0;	
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++) {
			r = pow((x0-i+n/2)/sigma,2) + pow((y0-j+n/2)/sigma,2);	
			kernel[i*n+j] = exp(-0.5*r)/sqrt(2*sigma*sigma*PI);	
			total += kernel[i*n+j];
		}
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++) 
			kernel[i*n+j] /= total;

	return kernel;
}




double* img_to_c_arr(cv::Mat m) {
	int M=m.rows, N=m.cols;
	double *data = new double[M*N];	
	for (int i=0;i<M;i++)
		for (int j=0;j<N;j++)
			data[i*N+j] = 1-m.at<unsigned char>(i,j)/255.;
	return data;
}

cv::Mat c_arr_to_img(double *arr, int M, int N) {
	cv::Mat m(M, N, CV_8UC1);
	int z;	
	for (int i=0;i<M;i++)
		for (int j=0;j<N;j++) {
			z = 255*(1-arr[i*N+j]);
			if (z>255) z=255;
			if (z<0)   z=0;	
			m.at<unsigned char>(i,j) = (unsigned char)z;
		}	
	return m;
}

double* convolution_manual(double *in, double *kernel, int M, int N, int k) {
	double *out = new double[M*N];	
	for (int i=0;i<M;i++) {
		for (int j=0;j<N;j++) {
			out[i*N+j] = 0;
			for (int l=0;l<k;l++) {
				for (int m=0;m<k;m++) {
					int t1=i+l-k/2, t2=j+m-k/2;
					if (t1<0 || t2<0 || t1>=M || t2>=N) continue;	
					out[i*N+j] += in[t1*N+t2] * kernel[l*k+m];
				}
			}
		}
	}
	return out;
}

double* convolution_fft(double *in, double *kernel, int M, int N, int k) {
	int n = k/2, ind, size = (M+k-1)*((N+k-1)/2+1);	
	double tmp;	
	double *padded_kernel = new double[(M+2*n)*(N+2*n)];
	double *padded_in = new double[(M+2*n)*(N+2*n)];
	double *out = new double[M*N];
	// create data for FFT, with last dimension N'/2+1
	fftw_complex *in_hat = new fftw_complex[size];
	fftw_complex *kernel_hat = new fftw_complex[size];

	for (int i=0;i<M+2*n;i++) {
		for (int j=0;j<N+2*n;j++) {
			padded_in[i*(N+2*n)+j] = 0;
			padded_kernel[i*(N+2*n)+j] = 0;
		}
	}
	for (int i=n;i<M+n;i++)
		for (int j=n;j<N+n;j++)
			padded_in[i*(N+2*n)+j] = in[(i-n)*N+(j-n)];

	for (int i=0;i<k;i++)
		for (int j=0;j<k;j++)
			padded_kernel[i*(N+2*n)+j] = kernel[i*k+j];

	// plan FFTs	
	fftw_plan F_in = fftw_plan_dft_r2c_2d(M+k-1, N+k-1, padded_in, in_hat, FFTW_ESTIMATE);
	fftw_plan F_kernel = fftw_plan_dft_r2c_2d(M+k-1, N+k-1, padded_kernel, kernel_hat, FFTW_ESTIMATE);	
	fftw_plan F_inv = fftw_plan_dft_c2r_2d(M+k-1, N+k-1, in_hat, padded_in, FFTW_ESTIMATE);

	// execute forward FFTs
	fftw_execute(F_in);
	fftw_execute(F_kernel);
	// multiply FFTs element wise and invert FFT
	for (int i=0;i<M+k-1;i++) {
		for (int j=0;j<(N+k-1)/2+1;j++) {
			ind = i*((N+k-1)/2+1)+j;
			tmp = in_hat[ind][0];
			in_hat[ind][0] = in_hat[ind][0]*kernel_hat[ind][0] - in_hat[ind][1]*kernel_hat[ind][1];	
			in_hat[ind][1] = tmp*kernel_hat[ind][1]+ in_hat[ind][1]*kernel_hat[ind][0];	
		}
	}

	fftw_execute(F_inv);
	// scale by size	
	for (int i=0;i<M+k-1;i++)
		for (int j=0;j<N+k-1;j++)
			padded_in[i*(N+k-1)+j] /= (M+k-1)*(N+k-1);

	// copy padded input into the output
	for (int i=k-1;i<M+k-1;i++)
		for (int j=k-1;j<N+k-1;j++)
			out[(i-k+1)*N+(j-k+1)] = padded_in[i*(N+k-1)+j];

	// clean memory	
	fftw_destroy_plan(F_in);
	fftw_destroy_plan(F_kernel);
	fftw_destroy_plan(F_inv);
	delete[] padded_in;
	delete[] padded_kernel;
	delete[] in_hat;
	delete[] kernel_hat;

	return out;
}

double* deconvolution(double* kernel, double *blur, int M, int N, int k) {
	// Solve B = K C, B=blurred img, C=clean img, K constructed from kernel	
	// initialize matrices	
	Eigen::MatrixXd K(M*N, (M+k-1)*(N+k-1));
	Eigen::MatrixXd B(M, N);

	// compute K	
	K.setZero();
	for (int i=0;i<M;i++) {
		for (int j=0;j<N;j++) {
			B(i,j) = blur[i*N+j];	
			for (int l=0;l<k;l++) {
				for (int m=0;m<k;m++) {
					K(i*M+j, (i+l)*(M+k-1)+j+m) = kernel[l*k+m];
				}
			}
		}
	}	
	Eigen::VectorXd B_t = B.reshaped<Eigen::RowMajor>();
	Eigen::BDCSVD<Eigen::MatrixXd> svd(K, Eigen::ComputeThinU | Eigen::ComputeThinV);
	svd.setThreshold(1e-5);	
	Eigen::VectorXd C = svd.solve(B_t);
	//Eigen::VectorXf C = K.colPivHouseholderQr().solve(B_t);

	double *out = new double[(M+k-1)*(N+k-1)];
	for (int i=0;i<M+k-1;i++)
		for (int j=0;j<N+k-1;j++)
			out[i*(N+k-1)+j] = C(i*(N+k-1)+j);

	return out;

}
/**
 * \brief 	compute a convolution
 * \details	use manual or fft based convolution to compute the convolution of 
 * 	kernel with the image
 * \param[in]	kernel		the k x k kernel used in the convolution 
 * \param[in]	k		the dimension of the kernel
 * \param[in]	use_fft		whether to use fft or not
 * \returns	a pointer to a newly created Image that approximates the deconvolution
 */
Image* Image::convolve(double* kernel, int k, bool use_fft) {
	int M=mat.rows, N=mat.cols;
	double *out;	
	if (use_fft) out = convolution_fft(img_to_c_arr(mat), kernel, M, N, k);
	else out = convolution_manual(img_to_c_arr(mat), kernel, M, N, k);
	Image *img = new Image(M,N);
	img->mat = c_arr_to_img(out,M,N);
	delete[] out;
	return img;
}
void apply_rotation(double* in, double* out, double angle) {
	angle *= PI / 180;	
	out[0] = in[0]*cos(angle) - in[1]*sin(angle);
	out[1] = in[0]*sin(angle) + in[1]*cos(angle);
}
double weighted_sum(double* grid, double* img, int M, int N) {
	int x_1, x_2, y_1, y_2;
	double a, b, x, y, total;	
	x = grid[0];
	y = grid[1];		
	x_1 = (int)x;
	x_2 = x_1 + 2*(x>=0)-1;	
	y_1 = (int)y;
	y_2 = y_1 + 2*(y>=0)-1;	

	a = abs(x-x_1);
	b = abs(y-y_1);
	if ((x_1+M/2)<0 || (x_1+M/2)>=M || (y_1+N/2)<0 || (y_1+N/2)>=N) 
		return 0.;	
	if ((x_2+M/2)<0 || (x_2+M/2)>=M || (y_2+N/2)<0 || (y_2+N/2)>=N) 
		return 0.;	

	total = 0;	
	total += (1-a)*(1-b)*img[(x_1+M/2)*N+y_1+N/2];
	total += (a)*(1-b)*img[(x_2+M/2)*N+y_1+N/2];
	total += (a)*(b)*img[(x_2+M/2)*N+y_2+N/2];
	total += (1-a)*(b)*img[(x_1+M/2)*N+y_2+N/2];

	return total;
}
double *rotate_image(cv::Mat data, double angle) {
	int M=data.rows, N=data.cols;	
	double* arr = img_to_c_arr(data);
	double* out = new double[M*N];
	// allocate memory and calculate grid coords	
	double ***grid = new double**[M];
	for (int i=0;i<M;i++) {
		grid[i] = new double*[N];	
		for (int j=0;j<N;j++) {
			grid[i][j] = new double[2];
			grid[i][j][0] = i-N/2;
			grid[i][j][1] = j-M/2;	
		}
	}
	double ***new_grid = new double**[M];
	for (int i=0;i<M;i++) {
		new_grid[i] = new double*[N];	
		for (int j=0;j<N;j++) {
			new_grid[i][j] = new double[2];
			apply_rotation(grid[i][j], new_grid[i][j], angle);
			out[i*N+j] = weighted_sum(new_grid[i][j], arr, M, N);
		}
	}
	// clean up memory	
	for (int i=0;i<M;i++) {
		for (int j=0;j<N;j++) {
			delete[] grid[i][j];
		}
		delete[] grid[i];	
	}
	delete[] grid;	
	for (int i=0;i<M;i++) {
		for (int j=0;j<N;j++) {
			delete[] new_grid[i][j];
		}
		delete[] new_grid[i];	
	}
	delete[] new_grid;	
	delete[] arr;
	return out;
}
/**
 * \brief 	rotate image by angle
 * \details	returns a new image rotated by angle degrees. points out of bounds
 * 	that get rotated into frame are filled with white
 * \param[in] 	angle	in degrees counter clockwise
 * \returns	a pointer to a newly created Image that has been rotated
 */
Image* Image::rotate(double angle) {
	double *out = rotate_image(mat, angle);	
	Image *img = new Image(height,width);
	img->mat = c_arr_to_img(out,height,width);
	delete[] out;
	return img;
}
/**
 * \brief 	approximate a deconvolution
 * \details	used regularized SVD to approximate the deconvolution of a clean
 * 	image with a k x k kernel
 * \param[in]	kernel		the k x k kernel used in the convolution 
 * \param[in]	k		the dimension of the kernel
 * \returns	a pointer to a newly created Image that approximates the deconvolution
 */
Image* Image::deconvolve(double* kernel, int k) {
	int M=height, N=width;	
	double *out = deconvolution(kernel, img_to_c_arr(mat), M, N, k);
	Image *img = new Image(M,N);
	img->mat = c_arr_to_img(out, M, N);
	return img;
}
