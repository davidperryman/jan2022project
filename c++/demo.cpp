#include "Image.h"

void print(double *data, int M, int N) {
	for (int i=0;i<M;i++) { 
		for (int j=0;j<N;j++) { 
			if (data[i*N+j]<1e-12) data[i*N+j] = 0;	
			std::cout<<std::fixed<<std::setprecision(2)<<data[i*N+j]<<" ";
		}
		std::cout<<"\n";	
	}
}
void print(fftw_complex *data, int M, int N) {
	for (int i=0;i<M;i++) { 
		for (int j=0;j<N;j++) { 
			std::cout<<std::fixed<<std::setprecision(4)<<data[i*N+j][0]<<" "<<data[i*N+j][1]<<"j   ";
		}
		std::cout<<"\n";	
	}
}

bool get_decision(std::string prompt) {
	char x;	
	std::cout<<prompt<<"(yes/no)\n";
	std::cin>>x;
	return (x=='y' || x=='Y');
}	
int main() {
	std::string res_dir = "../../data/";
	std::string output_dir = "../../output/";

	if (get_decision("do you want to make some ellipses?")) {
		std::cout<<"simulation test script\n";
		std::string fingerprint1 = "clean_finger";
		std::string load_image_path1 = res_dir + fingerprint1 + ".png";

		Image img1(load_image_path1);
		Image tmp1(img1.getheight(), img1.getwidth());

		//Necessary to call cast2double method
		img1.cast2double();
		//Testing apply bump method
		img1.apply_bump(tmp1, &gaussian, &elliptique_norm, 2, 0.7, true, output_dir+fingerprint1+"_");

		cv::waitKey(0);
		cv::destroyAllWindows();
	}	
	// --------------------------------------------------
	if (get_decision("do you want to make some ellipses again?")) {
		std::cout<<"restoration test script\n";
		std::string fingerprint = "weak_finger"; //name of the fingerprint to use
		std::string load_image_path = res_dir + fingerprint + ".png";

		Image img(load_image_path);
		Image tmp(img.getheight(), img.getwidth());

		//Necessary to call cast2double method
		img.cast2double();
		tmp.cast2double();

		unsigned int number_of_patches = 3000;
		unsigned int patches_size = 4;
		double alpha = 1; // scaling factor of the first ellipse of the restoration mask
		double beta = -10; // translation factor of the first ellipse of the mask
		unsigned int elliptic_mask_width = 20; //width of the ellipse mask, in pixel size
		bool output = true; // cv::imshow the mask and result, save those images in output_dir

		//restoration algorithm on img
		img.restoration(tmp, res_dir+"weak_finger.png", number_of_patches, patches_size, elliptic_mask_width, alpha, beta, output, output_dir + fingerprint);
		//Showing result
		tmp.print();
		//saving result in output_dir
		tmp.save(output_dir + fingerprint + "_restoration.png");

		cv::waitKey(0);
		cv::destroyAllWindows();
	}	
	if (get_decision("do you want to solve some matrices?")) {
		int M=8, N=10;
		double *data = new double[M*N];
		for (int i=0;i<M;i++)
			for (int j=0;j<N;j++)
				data[i*N+j] = i==j || (i+3)==j;
		std::cout<<"data\n";
		print(data, M, N);

		int k = 5;
		double *kernel = new double[k*k];
		for (int i=0;i<k;i++)
			for (int j=0;j<k;j++)
				kernel[i*k+j] = i==j;
		//kernel[(k/2)*k+k/2] = 1;
		kernel[3*k+2] = 1;
		kernel[4*k+2] = 1;
		for (int i=0;i<k;i++)
			for (int j=0;j<k;j++)
				kernel[i*k+j] /= 7;

		std::cout<<"\nkernel:\n";
		print(kernel, k, k);

		double* out = convolution_manual(data, kernel, M, N, k);
		std::cout<<"\nconv:\n";
		print(out, M, N);
		delete[] out;

		out = convolution_fft(data, kernel, M, N, k);
		std::cout<<"\nfft conv:\n";
		print(out, M, N);
		delete[] out;
		out = gaussian_kernel(k, 0, 0, 1e-12);
		std::cout<<"\ngaussian kernel:\n";
		print(out, k, k);
		delete[] out;

		out = gaussian_kernel(k, 0, 0, 1.);
		std::cout<<"\ngaussian kernel:\n";
		print(out, k, k);
		delete[] out;

		out = gaussian_kernel(7, 1, 2, 1.);
		std::cout<<"\ngaussian kernel:\n";
		print(out, 7,7);
		delete[] out;

		cv::Mat clean = cv::imread("../../data/clean_blur_ex.png", cv::IMREAD_GRAYSCALE);
		cv::imshow("", clean); cv::waitKey(0);

		cv::Mat blur = cv::imread("../../data/corrupted_blur_ex.png", cv::IMREAD_GRAYSCALE);
		cv::imshow("", blur); cv::waitKey(0);
		M = blur.rows;
		N = blur.cols;
		double *blur_arr;
		blur_arr = img_to_c_arr(blur);
		double *clean_approx = deconvolution(kernel, blur_arr, M, N, k);
		cv::Mat approx_img = c_arr_to_img(clean_approx, clean.rows, clean.cols);
		delete[] clean_approx;
		cv::imshow("", approx_img); cv::waitKey(0);

		cv:: Mat finger = cv::imread("../../data/clean_finger.png", cv::IMREAD_GRAYSCALE);
		double * rotated = rotate_image(finger, -40);
		delete[] rotated;
	}
	return 0;

}

